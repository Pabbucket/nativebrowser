/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import { StyleSheet, SafeAreaView, View} from 'react-native';
import IndexStore from './IndexStore';

export default class App extends Component {
  render() {
    return (
      <>
      <SafeAreaView style={ styles.SafeAreaViewTop } />
      <SafeAreaView style={ styles.SafeAreaView }>
        <View style={styles.container}>
          <IndexStore />
        </View>
      </SafeAreaView>
      </>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'stretch',
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'space-between',
  },
  SafeAreaViewTop: {
    flex:0,
    color: 'white',
    backgroundColor: 'grey',
  },
  SafeAreaView: {
    flex: 1, 
    backgroundColor: 'black',
  },
});

