import React, { Component } from 'react';
import { Provider } from './context';

import Toolbar from './components/Toolbar';
import Searchbar from './components/Searchbar';
import Navigation from './components/Navigation';
import WebViewWrapper from './components/WebViewWrapper';
import FooterToolbar from './components/FooterToolbar';


const urlPattern = /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/gm;
const urlMatch = (text) => text.match(urlPattern);
const processUri = (text) => !isHttp(text) ? text.replace( text, 'http://' + text ) : text;
const isHttp = (text) => text.match(/https\:\/\//i) || text.match(/http\:\/\//i);
const buildSearch = (text) => `https://duckduckgo.com/?q=${ replaceSpaces(text) }&t=hp&ia=web`;
const replaceSpaces = (text) => text.replace(/\s/, '+');

export default class IndexStore extends Component {
  constructor() {
    super();
    this.state = {
      uri: 'https://www.pumabrowser.com/',
      uriText: 'https://www.pumabrowser.com/',
      browsingHistory: [],
      canGoBack: false,
      canGoForward: false,
    };

    this.setUriText = (e) => {
      this.setState({ uriText: e });
    };
    
    this.navigateToUri = (e) => {
      const {text, canGoBack, canGoForward} = e.nativeEvent;
      const procesedUrl = !urlMatch(text) ? buildSearch(text) : processUri(text);

      this.setState({ 
        uri: procesedUrl,
        canGoBack,
        canGoForward,
      });

    };

    this._onNavigationStateChange = (webViewState) => {
      const newUrl = webViewState.url;

      if ( webViewState.loading === false && urlMatch(newUrl)) {
        this.setUriText(newUrl);
      }
    };

  }

  render() {

    const { 
      navigateToUri, 
      setUriText, 
      _onNavigationStateChange, 
    } = this;
    const  { 
      uri, 
      uriText,
      canGoBack,
      canGoForward,
    } = this.state;
    const values = {
      _onNavigationStateChange,
      navigateToUri, 
      setUriText, 
      uri, 
      uriText,
      canGoBack,
      canGoForward,
    };

    return (
      <Provider 
        value={values} >
        <Toolbar>
          <Searchbar />
          <Navigation />
        </Toolbar>
        <WebViewWrapper />
        <FooterToolbar text="Mega 🚀 Browser" />
      </Provider>
    );
  }
}