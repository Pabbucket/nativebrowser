import React from 'react';
import { StyleSheet, View, Button } from 'react-native';
import { Consumer } from '../context';


const Navigation = () => (
  <Consumer>  
    {
      ({ canGoBack, canGoForward }) => (
        <View style={styles.container}>
          <Button
            title="<"
            color={ canGoBack ? 'white' : 'grey'}
            accessibilityLabel="Navigate to previous page."
          />
          <Button
            title=">"
            color={ canGoForward ? 'white' : 'grey'}
            accessibilityLabel="Navigate to next page."
          />  
        </View>
      )
    }
  </Consumer>
  
);

export default Navigation;

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'row',
  },
});