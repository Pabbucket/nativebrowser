import React from 'react';
import { View, StyleSheet, TextInput, Button} from 'react-native';

import { Consumer } from '../context';

const Searchbar = () => (
  <View style={styles.inputField}>
    <Consumer>  
      {
        ({ navigateToUri, 
          setUriText, 
          uriText,
        }) => (
        
          <TextInput
            textContentType="URL"
            keyboardType="url"
            value={uriText}
            defaultValue="https://www.pumabrowser.com/"
            clearTextOnFocus={true}
            onChangeText={setUriText}
            onEndEditing={navigateToUri}
            style={styles.TextInput}
          />
        )
      }
    </Consumer>
  </View>
);

const styles = StyleSheet.create({
  inputField:{
    height: 30,
    borderRadius: 15,
    flexDirection:'row', 
    minWidth: 260,
    margin: 10, 
    padding:4, 
    alignItems:'center', 
    justifyContent:'center',
    backgroundColor: 'white',
  },
  TextInput: {
    flex:4,
    paddingLeft: 20,
    textAlign: 'left',
    backgroundColor: 'transparent',
  },
});

export default Searchbar;