import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

export default class Toolbar extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        {this.props.children}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'row',
    backgroundColor: 'black',
    alignItems: 'center',
    width: '100%',
    padding: 10,
  },
});
