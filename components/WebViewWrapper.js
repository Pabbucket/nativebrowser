import React from 'react';
import { ScrollView, Title } from 'react-native';
import { Consumer } from '../context';
import MyWebView from 'react-native-webview-autoheight';

const listenPushState = `window.history.pushState = window.postMessage(window.location.href, '*')`;

const patchPostMessageFunction = function() {
  var originalPostMessage = window.postMessage;

  var patchedPostMessage = function(message, targetOrigin, transfer) {
    originalPostMessage(message, targetOrigin, transfer);
  };

  patchedPostMessage.toString = function() {
    return String(Object.hasOwnProperty).replace('hasOwnProperty', 'postMessage');
  };

  window.postMessage = patchedPostMessage;
};

const patchPostMessageJsCode = '(' + String(patchPostMessageFunction) + ')();';

const WebViewWrapper = () => (
  <Consumer>  
    {
      ({ uri, _onNavigationStateChange}) => (
        <ScrollView style={{flex: 1}}>
          <MyWebView
            source={{uri: uri}}
            startInLoadingState={true}
            style={{height: 1500}}
            onNavigationStateChange={_onNavigationStateChange.bind(this)}
            onError={ (e) =>  <Title>{e}</Title> }
            ref={( webView ) => this.webView = webView}
            onMessage={ data => data.nativeEvent.data}
            injectedJavaScript={ patchPostMessageJsCode + listenPushState }
          />
        </ScrollView>
      )
    }
  </Consumer>

);

export default WebViewWrapper;

console.disableYellowBox = true;