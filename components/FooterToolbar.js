import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

const FooterToolbar = ({text}) => (
  <View style={styles.container}>
    <Text style={styles.text}>{text}</Text>
  </View>
);

export default FooterToolbar;

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: 30,
    backgroundColor: 'black',
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    padding: 13,
    color: 'white',
    fontWeight: 'bold',
    fontSize: 15,
  },
});
